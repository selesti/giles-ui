/*
* Basic Responsive iFrames.
*
* They always take the width of their parent
* They need a block-like display property.
*
* Simples.
*
* Note: add .dont-resize to your iFrame to bypass resizing
*/

import { debounce } from 'underscore';

(function(document) {

    'use strict';

    const forEachIframe = function (callback) {
        if (iframes.length > 0) {
            for (let i = 0; i < iframes.length; i++) {
                const iframe = iframes[i];

                if (iframe.className.indexOf('dont-resize') !== -1) {
                    iframes.splice(i, 1);
                } else {
                    callback(iframe);
                }
            }
        }
    };

    const resizeIframes = function () {
        forEachIframe(function(iframe) {
            let maxWidth = iframe.parentElement.getBoundingClientRect().width,
                ratio = iframe.height / iframe.width,
                height = maxWidth * ratio;

            iframe.style.height = height + 'px';
            iframe.style.width = '100%';
        });
    };

    let iframeDoms = document.getElementsByTagName('iframe'),
        iframes = Array.prototype.slice.call(iframeDoms);

    window.onload = resizeIframes();
    window.onresize = debounce(resizeIframes, 100);

})(document);
