# UI Components

This is a collection of UI components, starting points etc which can be imported to your project when needed.

For full explanation on all the components please refer to the [giles docs](https://giles-docs.selesti.com/docs/0.1/installation).

## Complete
- Hamburger Helper
- Scroll to Message Helper
- Nudge/Notification Helper
- Responsive iFrame
- Form Helper
- Cookie Consent
- Scroll to Element
- Ziggy - Named routes.

## 3rd Party Deps
- Axios
- Vue
- Vue Touch Events
- Babel Preset Env
- Core JS
- Cross Env
- ESLint
- ESLint Vue
- Glob All
- Laravel Mix
- Live Reload X
- Minimage
- PurgeCSS
- SassLint
- Tailwind
- Webpack
- Underscore

