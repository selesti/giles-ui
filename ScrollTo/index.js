const Scroll = function Scroll () {

    this.to = function (target, options) {
        target = target || '.message-to-read';

        this.options = Object.assign({
            duration: 400,
            offset: 30,
            easing: 'swing',
        }, options);

        return new Promise((resolve, reject) => {
            if (typeof Vue !== 'undefined') {
                return Vue.nextTick().then(() => {
                    return this.process(target, resolve, reject);
                })
            } else {
                return setTimeout(() => {
                    return this.process(target, resolve, reject);
                }, 100);
            }
        });
    };

    this.process = function (target, resolve, reject) {
        this.element = jQuery(target);

        if (!this.element.length) {
            return reject(`Scroll triggered - but could not find ${target}`);
        }

        if (!this.elementIsInView()) {
            return jQuery('html, body').animate({
                scrollTop: this.element.offset().top - this.options.offset,
            }, {
                duration: this.options.duration,
                easing: this.options.easing,
                complete: () => {
                    return resolve(this.element);
                },
            });
        }

        return resolve(this.element);
    };

    this.elementIsInView = function () {
        const minBoundry = window.scrollY;
        const maxBoundry = window.scrollY + window.innerHeight;

        return (
            this.element.offset().top > minBoundry &&
            this.element.offset().top < maxBoundry
        );
    };

};

export default new Scroll();