require('cookieconsent');

import Vue from 'vue';
import 'custom-event-polyfill';

let CookieConsent;

const CookieConsentManager = Vue.extend({

    name: 'CookieConsent',

    data() {
        return {
            instance: null,
            forceClosed: false,
            config: {
                palette: {
                    popup: { background: '#424b51' },
                    button: { background: '#3c8de0' },
                },
                theme: 'edgeless',
                content: {
                    message: 'Please review our privacy policy and cookie usage before continuing and agree using the button provided.',
                    link: 'Read our policy now',
                    dismiss: 'Agree',
                    href: '/privacy-policy',
                },
                law: {
                    regionalLaw: false,
                },
                location: false,
                onInitialise: status => this.onInitialise(status),
                onStatusChange: (status, previous) => this.onInitialise(status, previous),
                onPopupOpen: () => this.onPopupOpen(),
                onPopupClose: () => this.onPopupClose(),
            },
        }
    },

    created() {
        this.bootPlugin();
        document.addEventListener('cookie-consent:open', this.show);
        document.addEventListener('cookie-consent:close', this.hide);
    },

    computed: {

        isReady() {
            return !!this.instance;
        },

    },

    methods: {

        fireEvent(event, data) {
            const eventName = `cookie-consent:${event}`;

            document.dispatchEvent(
                new CustomEvent(eventName, {
                    detail: data,
                })
            );

            this.$emit(event, data);
        },

        bootPlugin() {
            cookieconsent.initialise(
                this.config,
                popup => this.pluginInitialised(popup),
                (error, popup) => this.pluginFailed(error, popup)
            );
        },

        pluginInitialised(popup) {
            this.instance = popup;
        },

        pluginFailed(error, popup) {
            console.error(error, popup);

            this.fireEvent('failed', {
                error, popup,
            });
        },

        onInitialise(status) {
            this.fireEvent('init', {
                status,
            });
        },

        onStatusChange(status, previous) {
            this.fireEvent('status', {
                status, previous,
            });
        },

        onPopupOpen() {
            setTimeout(() => {
                this.fireEvent('opened', {
                    popup: this.instance,
                });
            }, 500);
        },

        onPopupClose() {
            setTimeout(() => {
                this.fireEvent('closed', {
                    popup: this.instance,
                });
            }, 500);
        },

        isOpen() {
            if (this.isReady) {
                return this.instance.isOpen();
            }

            return true;
        },

        hasClosed() {
            return !!this.instance.getStatus();
        },

        hide() {
            this.instance.close();
            this.forceClosed = true;
        },

        show() {
            this.instance.open();
            this.forceClosed = false;
        },

        restore() {
            if (this.forceClosed) {
                this.instance.open();
                this.forceClosed = false;
            }
        },

    },

    watch: {

        instance(popup) {
            this.fireEvent('ready', {
                popup,
            });
        },

    },

});

const CookieConsentFactory = function(config = {}) {
    CookieConsent = window.CookieConsentComponent = new CookieConsentManager({
        data: {
            config,
        },
    });

    return window.CookieConsentComponent;
};

export default CookieConsentManager;

export {
    CookieConsent,
    CookieConsentFactory,
}
