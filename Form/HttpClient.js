import axios from 'axios';

const instance = window.axios || axios;

export default instance;
