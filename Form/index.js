import Errors from './Errors';
import http from './HttpClient';
import Scroll from '../ScrollTo';
import { deepCopy, arrayWrap } from '../util';

class Form {

    /**
     * Create a new form instance.
     *
     * @param {Object} data
     */
    constructor(data = {}) {
        this.element = null;
        this.busy = false;
        this.success = false;
        this.failed = false;
        this.errors = new Errors();
        this.transformData = null;
        this.originalData = deepCopy(data);

        Object.assign(this, data);

        return this;
    }

    /**
     * Wires the form up so we can automatically
     * handle form actions and methods.
     *
     * @param {Vue Component} context
     * @param {String} ref name of the form.
     */
    wire(context, ref) {
        context.$nextTick(() => {
            this.element = context.$refs[ref];
        });

        return this;
    }

    /**
     * Fill form data.
     *
     * @param {Object} data
     */
    fill(data) {
        this.keys().forEach(key => {
            this[key] = data[key];
        });

        return this;
    }

    /**
     * Get the form data.
     *
     * @return {Object}
     */
    data() {
        return this.keys().reduce((data, key) => ({...data, [key]: this[key]}), {});
    }

    /**
     * Get the form data keys.
     *
     * @return {Array}
     */
    keys() {
        return Object.keys(this).filter(key => !Form.ignore.includes(key));
    }

    /**
     * Start processing the form.
     */
    startProcessing() {
        this.errors.clear();
        this.busy = true;
        this.success = false;
        this.failed = false;

        return this;
    }

    /**
     * Finish processing the form.
     */
    finishProcessing() {
        this.busy = false;
        this.success = true;
        this.failed = false;

        return this;
    }

    /**
     * Clear the form errors.
     */
    clear() {
        this.errors.clear();
        this.success = false;
        this.failed = false;

        return this;
    }

    /**
     * Reset the form fields.
     *
     * @param {Array} which fields to reset - defaults to all.
     */
    reset(only = []) {
        only = arrayWrap(only);

        Object.keys(this)
            .filter(key => !Form.ignore.includes(key))
            .filter(key => only.indexOf(key) !== -1)
            .forEach(key => {
                this[key] = deepCopy(this.originalData[key])
            });

        return this;
    }

    /**
     * Submit the form via a GET request.
     *
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    get(url, config = {}) {
        return this.sendRequest('get', url, config);
    }

    /**
     * Submit the form via a POST request.
     *
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    post(url, config = {}) {
        return this.sendRequest('post', url, config);
    }

    /**
     * Submit the form via a PATCH request.
     *
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    patch(url, config = {}) {
        return this.sendRequest('patch', url, config);
    }

    /**
     * Submit the form via a PUT request.
     *
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    put(url, config = {}) {
        return this.sendRequest('put', url, config);
    }

    /**
     * Submit the form via a DELETE request.
     *
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    delete(url, config = {}) {
        return this.sendRequest('delete', url, config);
    }

    submit({ method, url, config } = {}) {
        method = method || this.element.getAttribute('method') || 'post';
        url = url || this.element.getAttribute('action') || window.location.href;

        return this.sendRequest(method, url, config);
    }

    /**
     * Submit the form data via an HTTP request.
     *
     * @param  {String} method (get, post, patch, put)
     * @param  {String} url
     * @param  {Object} config (axios config)
     * @return {Promise}
     */
    sendRequest(method, url, config = {}) {
        this.startProcessing();

        let data = method === 'get' ? { params: this.data() } : this.data();

        if (this.transformData) {
            data = this.transformData(data);
        }

        return new Promise((resolve, reject) => {
            http.request({url: url, method, data, ...config})
                .then(response => {
                    this.finishProcessing();

                    Scroll.to();

                    resolve(response);
                })
                .catch(error => {
                    this.busy = false;
                    this.failed = true;

                    if (error.response) {
                        this.errors.set(
                            this.extractErrors(error.response),
                        );

                        Scroll.to();
                    }

                    reject(error);
                })
        })
    }

    /**
     * Extract the errors from the response object.
     *
     * @param  {Object} response
     * @return {Object}
     */
    extractErrors(response) {

        // If we haven't got a handled response, provide a default message.
        if (!response.data || typeof response.data !== 'object') {
            return { error: Form.errorMessage };
        }

        // If its an array of errors all is good!
        if (response.data.data && typeof response.data.data === 'object') {
            return response.data.data;
        }

        // If there's a single message, we'll show that too!.
        if (response.data.data && typeof response.data.data === 'string') {
            return { error: response.data.data };
        }

        // As above ^
        if (response.data.message) {
            return { error: response.data.message };
        }

        return { ...response.data };
    }

}

Form.errorMessage = 'Something went wrong. Please try again.';
Form.ignore = ['busy', 'success', 'errors', 'originalData'];

export default Form;
